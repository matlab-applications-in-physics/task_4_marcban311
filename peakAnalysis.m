%MATLAB 2020b
%name: peakAnalysis
%author: marcban311
%date: 06.12.2020
%version: 


clear;
clc;

% Open the file
myFileName = 'Co60_1350V_x20_p7_sub.dat';
myFileId=fopen(myFileName);

% Define size of the RAM for buffer
blockSize = 1000000;
i=0;
vector_value=0; 

%feof read one line at a time to the end of the file myFile
%this part of code is quiet similar to jakgre code becouse of problems 
while ~feof(myFileId)
    
    fseek(myFileId, blockSize*i, -1);
    data = fread(myFileId,blockSize,'uint8');

    [value,edges] = histcounts(data,'BinLimits',[0,250],'BinWidth', 1,'Normalization','pdf');
    vector_value = vector_value + value;
    i=i+1;
end

histogram('BinEdges',edges,'BinCounts',vector_value);
xlabel('time[s]');
ylabel('Voltage[V]');

%accualy i don know how this works but it works 
%gfc as fig
saveas(gcf,'Co60_1350V_x20_p7_sub_histogram.pdf');